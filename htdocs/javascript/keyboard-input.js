// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// NOP's very own keyboard input system
// Using a mix of keyup, keydown and keypress in an attempt to
// please all browsers' quirks at once...

var brokenBrowserKeyCode;

if (window.event)
{
    brokenBrowserKeyCode = window.event.keyCode;
}

$(document).ready(function ()
{
    //We disable the browser's input history here
    $("#input").attr("autocomplete", "off" );

    var userInput = '';
    var history = [];
    var index = history.length;
    var defaultVocab = ['walk','tweet', 'talk', 'inventory', 'look', 'examine', 'accept',
                        'strongly-agree', 'agree', 'no-opinion', 'disagree', 'strongly-disagree'];

    $("#input").keyup(function (evt)
    {
        userInput = $(this).val();
    });

    $('#wrapper-game').delegate('.entity', 'click', function()
    {
        // Checks the names of all the exits in current node
	    var exits = [];
	    for (exit in game.node.exits)
	    {
	        exits.push(game.node.exits[exit].name);
	    }

        var talk_choices=[];
        for (var i in game.talk_node.choices)
        {
            if (game.talk_node.choices[i].keywords!=null)
            {
                for (var n in game.talk_node.choices[i].keywords)
                {
                    talk_choices.push(game.talk_node.choices[i].keywords[n]);
                }
            }
        }

	    // If we click on an existing exits, the user input is
	    // replaced by 'walk @exit'
	    if (jQuery.inArray($(this).text(),exits) != -1)
	    {
	        userInput = 'walk @'+$(this).text();
	    }
	    // If we click on an existing talk choice, the user input is
	    // replaced by '@<talking_to> choice'
	    else
        {
            if (game.talking_to && jQuery.inArray($(this).text(),talk_choices) != -1)
	        {
	            userInput = '@'+game.talking_to+' '+$(this).text();
	        }
            else
	        {
	            if (userInput != '')
                {
                    userInput = userInput.trim(' ')+' @'+$(this).text()+' ';
                }
                else
                {
                    userInput = '@'+$(this).text()+' ';
                }
            }
        }
        document.area.input.value = userInput;
        $('#input').focus();
        $("#input").putCursorAtEnd();
    });

    $(document.documentElement).keypress(function (evt)
    {

        if (evt.keyCode == 13 || brokenBrowserKeyCode == 13)
        {   
            
            // What is your vocabulary?
            server_call("inventory", {"uid": game.player_id},
                function (data)
                {
                    var gold = false;

                    // Are you gold?
                    for (var i in game.inventory)
                    {
                        if (game.inventory[i].name == "Golden Medallion")
                        {
                            gold = true;
                        }
                    }

                    // twwuit zstyl FTW!
                    if (userInput.length < 140 || gold == true)
                    {
                        var t=JSON.parse(data);
                        game.vocab=defaultVocab.concat(t.vocab);
                        var action=userInput.trim(' ').split(' ')[0];                    
                    
                        game.parse_talk(userInput);
                        
                        if ($.inArray(action, game.vocab) != -1)
                        {
                            //We clean the white spaces
			    var cleanUserInput = userInput.trim(' ').replace(/  +/g,' ');;
			    game.parse(cleanUserInput);
                        }
                        
                        document.area.input.value="";
                        history.push(userInput);
                        index = history.length;
                        userInput = '';
                    }
                    else
                    {
                        error("You are surpringly limited to 140 characters");
                        document.area.input.value="";
                    }
                });

            return false; // Prevent Return to submit input
        }; 
    });

    $(document.documentElement).keydown(function (evt)
    {
        
        // Prevent TAB key to change focus
        if ((evt.keyCode == 9))
        {
            evt.preventDefault();
        };

        // Command history navigation
        if ((evt.keyCode == 38) && (index > 0))
        {

                evt.preventDefault();
                index -= 1;
                document.area.input.value=history[index];
                $("#input").putCursorAtEnd();

        };
        
        if ((evt.keyCode == 40) && (index < history.length - 1))
        {
                evt.preventDefault();
                index += 1;
                document.area.input.value=history[index];
                $("#input").putCursorAtEnd()
        };  


    });
});

