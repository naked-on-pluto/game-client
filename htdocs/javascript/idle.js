// idle.js
// (C) 2009 Alexios Chouchoulas
// (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

 
var _idleTimeout;
var _awayTimeout;
 
var _idleNow = false;
var _idleTimestamp = null;
var _idleTimer = null;
var _awayNow = false;
var _awayTimestamp = null;
var _awayTimer = null;
 
function setIdleTimeout(ms)
{
    _idleTimeout = ms;
    _idleTimestamp = new Date().getTime() + ms;
    if (_idleTimer != null) {
	clearTimeout (_idleTimer);
    }
    _idleTimer = setTimeout(_makeIdle, ms + 50);
}
 
function setAwayTimeout(ms)
{
    _awayTimeout = ms;
    _awayTimestamp = new Date().getTime() + ms;
    if (_awayTimer != null) {
	clearTimeout (_awayTimer);
    }
    _awayTimer = setTimeout(_makeAway, ms + 50);
}
 
function _makeIdle()
{
    var t = new Date().getTime();
    if (t < _idleTimestamp) {
	_idleTimer = setTimeout(_makeIdle, _idleTimestamp - t + 50);
	return;
    }
    //debug('** IDLE **');
    _idleNow = true;
 
    try {
	if (document.onIdle) document.onIdle();
    } catch (err) {
    }
}
 
function _makeAway()
{
    var t = new Date().getTime();
    if (t < _awayTimestamp) {
	_awayTimer = setTimeout(_makeAway, _awayTimestamp - t + 50);
	return;
    }
    //debug('** AWAY **');
    _awayNow = true;
 
    try {
	if (document.onAway) document.onAway();
    } catch (err) {
    }
}
 
 
function _active(event)
{
    var t = new Date().getTime();
    _idleTimestamp = t + _idleTimeout;
    _awayTimestamp = t + _awayTimeout;
    //debug('not idle.');
 
    if (_idleNow) {
	setIdleTimeout(_idleTimeout);
    }
 
    if (_awayNow) {
	setAwayTimeout(_awayTimeout);
    }
 
    try {
	//debug('** BACK **');
	if ((_idleNow || _awayNow) && document.onBack) document.onBack(_idleNow, _awayNow);
    } catch (err) {
    }
 
    _idleNow = false;
    _awayNow = false;
}
 
function idle_check()
{
    var doc = $(document);
    doc.ready(function(){
            doc.mousemove(_active);
            try {
                doc.mouseenter(_active);
            } catch (err) { }
            try {
                doc.scroll(_active);
            } catch (err) { }
            try {
                doc.keydown(_active);
            } catch (err) { }
            try {
                doc.click(_active);
            } catch (err) { }
            try {
                doc.dblclick(_active);
            } catch (err) { }
        });
}
 
$(document).ready(function ()
{
    idle_check();

    setIdleTimeout(180000); // 3 minutes
    setAwayTimeout(300000); // 5 minutes

    document.onIdle = function()
    {
	feed_it('main', 'EVr14', 
		'<span class="my entity">EVr14</span>'
		+' @<span class="their entity">'
		+game.player_name+'</span> '
		+'Idling is not recommended for a positive customer experience.'
		, 'important');
    }

    document.onAway = function()
    {
	if (facebook_enabled == true)
	{
	    game.fb.logout();
	}

	debug("Elvis has left the building.");
    }

    document.onBack = function(isIdle)
    {
	if (isIdle)
	{
	feed_it('main', 'EVr14', 
		'<span class="my entity">EVr14</span>'
		+' @<span class="their entity">'
		+game.player_name+'</span> '
		+'You\'re back at last! I\'m so pleased. Remember, sharing brings people closer together ♥'
		, null);
	}
    }
});
