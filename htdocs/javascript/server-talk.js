// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

$.ajaxSetup ({  
    cache: false  
});  


function server_call(name,args,f)
{
    args.function_name = name;
    $.get("main", args, f);
};

// not sure where to put this, needed by pluto and fb ->
 
// honor quoted bits of text in the splitting, ie. splits something
// like "blah blah \"hello there\" blah" into 
// ["blah", "blah", "hello there", "blah"]
function quote_split(str,delim)
{
    var s=str.split(delim);
    var out=[];
    var collect="";
    var collecting=false;
    for (i in s)
    {
        if (!collecting) 
        {
            if (s[i][0]=="\"")
            {
                if (s[i][s.length-1]=="\"")
                {
                    out.push(s[i].substr(1,s[i].length-2));
                }
                else
                {
                    collecting=true;
                    collect=s[i].substr(1,s[i].length-1);
                }
            }
            else
            {
                out.push(s[i]);
            }
        }
        else
        {
            if (s[i][s[i].length-1]=="\"")
            {
                collect+=" "+s[i].substr(0,s[i].length-1);                
                out.push(collect);
                collecting=false;
            }
            else
            {
                collect+=" "+s[i];
            }
        }
    }
    return out;
}
