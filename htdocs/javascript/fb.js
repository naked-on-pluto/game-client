// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function fb_interface(appid,refresh)
{   
    if (appid!="") 
    {
        $(document).ready(function()
	{
	    FB.init({appId: appid, status: true, cookie: true, xfbml: true});
	});
    }

    this.refresh=refresh;
    this.friends=[];
    this.current_friend=-1;
    this.accessToken=false;
    this.uid=false;

    if (appid!="")
    {
        this.data = {
            me: {},
            likes: [],
            people: [],
            books: [],
	        movies: [],
            mylocations: [],
            locations: [],
            apps: [],
            peoplelikes: []
        }
    }
    else
    {
        this.current_friend=0;
        // some test data
        this.data = {
            me: { id:99, first_name:"Bob", gender:"male",
                  last_name:"Bloggs", name:"Bob Bloggs"},
            mypic: "",
            people: ["FredFoo","JimJobble","BobbyBoobar"],
            peoplelikes: [
                {name:"FredFoo",likes:["Jam", "Honey"]},
                {name:"JimJobble",likes:["Cars", "Flowers"]},
                {name:"BobbyBoobar", likes:["Red", "Green"]}
            ],
            apps: ["Naked on Pluto"],
            books: ["The Dictionary", "The DaVinci Code"],
	        movies: ["The Third Man"],
            mylocations: ["Sweet Home"],
            locations: ["Barcelona, Spain"],
            likes: ["One","Two","Three"]
        }
        refresh("me",this.data.me);
    }

    /////////////////////////////////////////////////////////////////
    // the login stuff

    this.login=function()
    {
        // Clean page from intro texts/menu
        $('#wrapper-intro').css('display','none');
        $('#footer-intro').css('display','none');
        // Show the loading screen
        newLoaderKeyword();
        $('html').css('background-color', '#3b5998');
        $('html').css('background-image', 'url(images/pluto_logo_transparent.png)');
        $('html').css('background-repeat', 'repeat');
        $("#loader").css("display","inline");

        var fb=this;

        FB.getLoginStatus(function(response) {
            if (response.status=="connected") {
                facebook_status('I am now naked on Pluto. <input type="button" value="Logout" onClick="game.fb.logout();">');
		fb.uid = response.authResponse.userID;
		fb.accessToken = response.authResponse.accessToken;
                fb.suck();
	    }
	    else
	    {
		FB.login(function(response) {
			if (response.authResponse) {
			    fb.accessToken = response.authResponse.accessToken;
			    facebook_status('I am now naked on Pluto. <input type="button" value="Logout" onClick="game.fb.logout();">');
			    fb.suck();
			} else {
                window.location="http://naked-on-pluto.net";
			    //facebook_status(":( the login didn't work! " + '<input type="button" value="Login to facebook" onClick="login();">');
			}
		    }, {scope:'user_about_me,friends_about_me,user_activities,friends_activities,user_education_history,friends_education_history,user_events,friends_events,user_groups,friends_groups,user_hometown,friends_hometown,user_interests,friends_interests,user_likes,friends_likes,user_location,friends_location,user_notes,friends_notes,friends_website,user_work_history,friends_work_history'});
	    }		
	    });
    }
    

    // ... and the logout
    this.logout=function()
    {
        FB.logout(function()
        {
            window.location="http://naked-on-pluto.net";
            // Turns game page into intro page
//            $('html').css('background-image', 'url(images/theatrical_opening.jpg)');
//            $('html').css('background-color', '#FFF');
//            $('#wrapper-game').css('display','none');
//            $('#wrapper-intro').css('display','block');
//            $('#footer-intro').css('display','inline');
        });
    };
        


    ////////////////////////////////////////////////////////////////
    // get the data from the api

    this.safe_add = function(whence,data)
    { 
	    if (data!=undefined) 
        {
            debug("adding "+data+" to "+whence);
            this.data[whence].push(data);
        }
    }

    this.suck_likes=function(name,likes)
    {
	    var fb=this;
        var all=[];
        //debug(object_to_string(things));
	    $.each(likes,function(index,thing){
            all.push(thing.name);
            // debug(thing.category);
		    if (thing.category=="Books") fb.safe_add("books",thing.name);
		    if (thing.category=="Movie" || thing.category=="Film") 
			    fb.safe_add("movies",thing.name);		
		    if (thing.category=="Application") fb.safe_add("apps",thing.name);
	    });
        debug(name+" has "+all.length+" likes");
        if (all.length>0)
        {
            fb.data["peoplelikes"].push({
                name: name,
                likes: all});
        }
    }

    this.suck_from_friend = function(name,friend)
    {
	    if (friend.location!=undefined) this.safe_add("locations",friend.location.name);
        if (friend.hometown!=undefined) this.safe_add("locations",friend.hometown.name);

        if (friend.work!=undefined)
        {
            $.each(friend.work,function(work){
			    if (work.employer!=undefined)
                {
                    this.safe_add("locations",work.employer.name);
                }
            });
        }

        if (friend.education!=undefined)
        {
            $.each(friend.education,function(education){
                if (education.school!=undefined)
                {
                    this.safe_add("locations",education.school.name);
                }
            });
        }
        var fb=this;
        FB.api('/'+friend.id+'/likes', function(likes){
            if (likes.data!=undefined) fb.suck_likes(name,likes.data);
        });
    }

    this.suck_friend=function(friend) 
    {
	    var fb=this;
        var name=friend.name.replace(/\s/g, "");
        fb.safe_add("people",name);
 
	    FB.api("/"+friend.id, function(friend) {
            fb.suck_from_friend(name,friend)
	    });	
    }

    this.poll = function()
    {
        if (this.current_friend>-1 && appid!="")
        {
            if (this.current_friend<this.friends.length)
            {
                newLoaderKeyword();
                this.suck_friend(this.friends[this.current_friend]);
                this.current_friend=this.current_friend+1;
            }
            else
            {
                // sucking is done, time to show the UI
                clean_feeds(); // we remove any leftover crap from previous session
                $('#loader').css('display','none');
                $('#wrapper-game').css('display','block');
                $('#footer-game').css('display','inline');
                $('#input').focus();    
                this.current_friend=-1;
            }
        }       

        if (appid=="" && this.current_friend>-1)
        {
            clean_feeds();
            $('#loader').css('display','none');
            $('#wrapper-game').css('display','block');
            $('#footer-game').css('display','inline');
            $('#input').focus();    
            this.current_friend=-1;
        }
    }

    this.suck_friends=function()
    {
	var fb=this;
        FB.api('/me/friends', function(friends)
        {
            fb.friends=friends.data;
            fb.current_friend=0;
        });
    }

    this.suck_me=function()
    {
        var fb=this;
        FB.api('/me', function(response) {

            fb.data.me = response;
            fb.refresh("me",response);

            FB.api('/me/likes', function(likes){
                fb.data.likes=[];
	            $.each(likes.data,function(index,thing){
                    fb.data.likes.push(thing.name); 
	            })});

	        if (response.location!=undefined) fb.safe_add("mylocations",response.location.name);
            if (response.hometown!=undefined) fb.safe_add("mylocations",response.hometown.name);

            if (response.work!=undefined)
            {
                $.each(response.work,function(work){
			        if (work.employer!=undefined)
                    {
                        fb.safe_add("mylocations",work.employer.name);
                    }
                });
            }

            if (response.education!=undefined)
            {
                $.each(response.education,function(education){
                    if (education.school!=undefined)
                    {
                        fb.safe_add("mylocations",education.school.name);
                    }
                });
            }

            fb.suck_friends();
        });
    }
    
    this.suck = function()
    {
        debug("looking at facebook data...");
        this.suck_me();
    }

    //////////////////////////////////////////////////////////////////
    // stuff for getting data out

    // look for words like #something#xx# and replace with fb data
    this.substitute_fake_info = function(str)
    {
	var that = this;
	var spoofed = '';
	return str.replace(/(#[a-z]*#[0-9]*#[0-9]*#|#[a-z]*#[0-9]*#|#[a-z]*)/g, function(spoof)
        {
	        //if (spoof.indexOf('person'))
            //{
            //    return '@<span class="their entity">'+that.get_data_from_string(spoof)+'</span>';
            //}
            //else
            //{
                return that.get_data_from_string(spoof);
            //}
        });
    }

    this.safe_get = function(type,index)
    {
        var len=this.data[type].length;
        if (len>0) return this.data[type][index%len];
        else return "undefined!";
    }

    this.get_data_from_string = function(str)
    {
        var toks=quote_split(str,"#");
        var t=toks[1];
        if (t=="myid") return this.data.me.id;
        if (t=="myname") return this.data.me.name.replace(/\s/g, "");
        var i=parseInt(toks[2]);
        var d="";
        if (toks.length>2) d=toks[3];
        if (t=="person") return this.safe_get("people",i)+d;
        if (t=="mylocation") return this.safe_get("mylocations",i)+d;
        if (t=="location") return this.safe_get("locations",i)+d;
        if (t=="movie") return this.safe_get("movies",i)+d;
        if (t=="book") return this.safe_get("books",i)+d;
        if (t=="like") return this.safe_get("likes",i)+d;
        if (t=="app") return this.safe_get("apps",i)+d;
        if (t=="friendlike") 
        {
            //alert(toks.length);
            var friend=this.safe_get("peoplelikes",i);
            if (toks.length==4)
            {
                return friend.name+d;
            }
            else
            {
                var likes=friend.likes;
                var y=parseInt(d);
                if (toks.length>3) d=toks[4];
                return likes[y%likes.length]+d;
            }
 
        }
     }

    //if (appid!="") this.init();
}
