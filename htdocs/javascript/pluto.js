// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var facebook_enabled=true;
var naked_on_pluto_debug=true;

// takes an array of strings and concatenates them together
// given the start and stop (stop is num elements from the end
// where 0 is the end)
function build_name(args,start,stop)
{
    var name = "";
    var end=args.length-stop;
    for (var i=start; i<end; i++)
    {
        name+=args[i];
        if (i!=end-1) name+=" ";
    }
    return name;
}

// match lowercase or any word
function flexieq(a,b)
{
    a=a.toLowerCase();
    b=b.toLowerCase();
    if (a==b) return true;
    var words=b.split(" ");
    for (var i in words)
    {
        if (a==words[i]) 
        {
            return true;
        }
    }
    return false;
}

function str_in_arr(str, arr)
{
    var s=str.toLowerCase(str);
    for (var i in arr)
    {
        if (s==arr[i].toLowerCase()) return true;
    }
    return false;
}

// deref look
// So far javascript was fun and then, this.
function deref_look(obj)
{
    this.look = null;
    obj.look(['@whatever']);
}

// if string contains a word beginning with @ then return it, 
// otherwise return false
function is_tweet(str)
{
    var s=str.split(" ");
    for (var i in s)
    {
        if (s[i][0]=="@") return s[i].slice(1);
    }
    return false;
}

// removes word beginning with @ from a string
function remove_tweet(str)
{
    var s=str.split(" ");                                                     
    var r="";                                                                 
    for (i in s)                                                              
    {                                                                         
        if (s[i][0]!="@")                                                     
        {                                                                     
            r+=s[i];                                                          
            if (i<s.length-2) r+=" ";                                         
        }                                                                     
    }                                                                         
    // need to convert strings like '@gamebot2000 accept' into 'accept'
    //return str.replace(/@/g,'');
    return r;
}

// This is really bad...
function remove_tweet2(str)
{
    return str.replace(/@/g,'')
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// the main game object

function game_world()
{
    this.node = false;
    this.player_name = "";
    this.player_id = 0;
    this.entity_id = 0;
    this.inventory = 0;
    this.ready=false;
    this.talking_to=false;
    this.talk_pos=0;
    this.talk_node=false;
    this.fb = 9999;
    this.last_messages=[];

    this.startup = function()
    {
        if (facebook_enabled)
        {
            // the API key is stored in the untracked api-key.js
            // echo "var naked_api_key = '1234567890abcdef';" > api-key.js
            this.fb = new fb_interface(naked_api_key, this.fb_refresh);
        }
        else
        {
            // call with some fake stuff
            this.fb = new fb_interface("", this.fb_refresh);
        }
    }

    this.fb_refresh = function(name,data)
    {
        debug(object_to_string(data));
        if (name=="me")
        {
            var name = data.name.replace(/\s/g, "");
            game.player_name=name;
            game.player_id=data.id;
            var gender = "female"; // default(!)
            if (data.gender!=null) gender=data.gender;

            // login to the game server, the uid and first name is the only data we keep on the user
            server_call("login", 
                        { "id": data.id, "name": name, "gender": gender }, 
                        function(data) 
                        {
                            //alert(data);
                            var o = JSON.parse(data);
                            game.entity_id=o["entity-id"];
                            if (game.entity_id==-1) 
                            {
                                window.location="full.htm";
                            }
                            else
                            {
                                debug("num active users="+o["active-users"]);
                                game.ready=true;
                                game.refresh();
                            }
                        });
        }
    }

    // was this message displayed last time, or is it
    // in the "more" list?
    this.is_duplicate_message = function(msg,more)
    {
        for (var i in more)
        {
            var other=more[i];
            if (msg.to == other.to && 
                msg.from == other.from &&
                msg.txt == other.txt) return true;
        }
        for (var i in this.last_messages)
        {
            var other=this.last_messages[i];
            if (msg.to == other.to && 
                msg.from == other.from &&
                msg.txt == other.txt) return true;
        }
        return false;
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // load the node, or a new one if dir is not "none"
    this.load_node = function(name,fn)
    {        
        // if we don't have it already
        if (this.ready)
        {
            this.ready=false;
            // timeout the loading after 20 secs
            var t = setTimeout("game.ready=true;",20000); 

            server_call("inventory",{"uid": this.player_id},
                        function (data)
                        {
                            var t=JSON.parse(data);
                            game.inventory=t.inventory;
                            game.clothes=t.clothes;
                            game.vocab=t.vocab;
                            game.money=t.money;
                        });

            server_call("get-node",{"uid": this.player_id,
                                    "name": name}, 
                        function (data) 
                        {  
                            clearTimeout(t);
                            var node = JSON.parse(data);
                            if (!node)
                            {
                                //alert(data);
                                error("hmm, couldn't load "+name);
                            }
                            else
                            {
                                game.ready=true;
                                //alert(data);
                                if (node.objects==null) node.objects=[];
                                if (node.exits==null) node.exits=[];
                                if (node.messages==null) node.messages=[];
                                
                                // filter away the duplicate messages in the list, 
                                // or from the messages from last time. this is needed
                                // as the server sends all messages sent within 10 seconds
                                // and we update every 5 seconds - so all messages should 
                                // be seen, but most times we will get them twice in a row...
                                var filtered=[];
                                for (var i in node.messages)
                                {
                                    if (!game.is_duplicate_message(node.messages[i],filtered))
                                    {
                                        filtered.push(node.messages[i]);
                                    }
                                }
                                game.last_messages=node.messages;
                                node.messages=filtered;
                               
                                game.node=node;
                                game.update_chat();
                                if (fn!=null){fn(game)};
                            }
                        }); 
        
        } 
    };

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // refresh the world, to get any changes from the server
    this.refresh = function()
    {
        this.load_node("same",null);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // look for an object in the inventory
    this.carrying = function(name)
    {
        if (this.inventory!=null)
        {
            for (var i in this.inventory)
            {
                if (name==this.inventory[i].name) return true;
            }
        }
        return false;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // given a name, look for the object in the current location
    // and the inventory, and return it's id number

    this.find_inventory = function(name)
    {
        if (this.inventory!=null && name[0]=="@")
        {
            name=name.slice(1);
            for (var i in this.inventory)
            {
                if (flexieq(name,this.inventory[i].name)) 
                {                
                    return this.inventory[i].id;
                }
            }
        }
        return -1;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // same as above, but just searches the current location

    this.find_object = function(name)
    {
        if (name[0]=="@")
        {
            name=name.slice(1);
            var obs=this.node.objects;
            for (var i in obs)
            {
                if (flexieq(name,obs[i].name)) return obs[i].id;
            }
        }
        return -1;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // gets the name from the id

    this.name_of_object = function(id)
    {
        var obs=this.node.objects;
        if (this.inventory!=null) obs=obs.concat(this.inventory);
        for (var i in obs)
        {
            if (id==obs[i].id) return obs[i].name;
        }
        return "unknown";
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // gets the FB id from the object name

    this.uid_of_name = function(name)
    {
        var objects=this.node.objects;
        var friends=this.fb.friends;

        for (var obj in objects)
        {
            if (name==objects[obj].name) return objects[obj].uid;
        }
        for (var friend in friends)
        {   
            if (name==friends[friend].name.replace(/ /g, '')) return friends[friend].id;
        }

        return "unknown";
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // check if we're moustache spoofing

    this.stache_check = function()
    {
        var objects=this.node.objects;

        for (var obj in objects)
        {   
            if (this.player_id==objects[obj].uid) return objects[obj].name;
        }

        return this.player_name;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // move around the world
    this.move = function(to)
    {
        var node = this.node;
        if (!node)
        {
            error("Oops - waiting for location to load...");
            feed_it('main', 'EVr14', this.player_name + " caused an error to occur!", null);
        }
        else
        {
            // search for an outward edge in this direction
            for (var i in node.exits)
            {
                if (flexieq(to,node.exits[i].name))
                {
                    if (node.exits[i].name=="AreYouSureYouWantToDestroy")
                    {
                        server_call("vote-for",{"uid": this.player_id},
                                    function()
                                    {
                                        window.location="votes.htm";
                                    }); 
                    }
                    if (node.exits[i].name=="AreYouSureYouWantToKeep")
                    {
                        server_call("vote-against",{"uid": this.player_id},
                                    function()
                                    {
                                        window.location="votes.htm";
                                    }); 
                    }
                       
                    if (!node.exits[i].locked) // this is also checked on the server
                    {
                        this.talking_to=false;
                        //feed_it('main', this.fb.data.me.id, "I moved to " + node.exits[i].name + " and I'm looking around...", null);
                        this.load_node(node.exits[i].name, deref_look);
                        return;
                    }
                    else
                    {
                        error('<span class="locked-exit">BLOCKED WAY:</span> '+node.exits[i].blocked);
                    }
                }
            }
            //error("the exit is blocked. You can't go in that direction...");
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // display current chat
    this.update_chat = function()
    {
        var node = this.node;

        if (node.messages.length!=0)
        {
            for (var i in node.messages)
            {
                var msg=node.messages[i];

                // check if we are being summoned to talk
                if (msg.meaning=="talk" && this.entity_id==msg.to)
                {
                    this.talking_to = this.name_of_object(msg.from);
                    this.talk_pos = 0;
                    this.talk();
                }

                var txt = msg.txt;

		        var targetName = this.name_of_object(msg.to);
		        if (targetName == 'unknown' || txt.toLowerCase().indexOf(this.name_of_object(msg.to).toLowerCase())!=-1)
		        {
		            targetName = '';
		        }
                else
                {
                    targetName = '@<span class="their entity">'+this.name_of_object(msg.to)+'</span>';
                }
                
                var html = '<span class="my entity">'
                        +this.name_of_object(msg.from)+'</span> '+targetName+' '+txt;

                var myFaceId = this.name_of_object(msg.from);

                if (msg.to == game.entity_id)
                {
                    // always send spam to the public feed
                    if (msg.meaning=="spam")
                    {
                        feed_it('pub', myFaceId, html, 'talk');
                    }
                    else
                    {
                        // send important messages to the central feed
                        if (msg.meaning=="important")
                        {
                            feed_it('main', myFaceId, html, msg.meaning);
                        }
                        else
                        {
                            // normal messages to the private feed
                            feed_it('priv', myFaceId, html, msg.meaning);
                        }
                    }
                }
                else if (node.messages[i].meaning != 'important')
                {
                    // messages to everyone else (except important ones)
                    // go to the public feed
                    feed_it('pub', myFaceId, html, msg.meaning);
                }
             }
        };
    }    

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // display information on our current location
    this.look = function(args)
    {
        
	if ((args[1]) && (remove_tweet2(args[1]).toLowerCase() != this.node.name.toLowerCase()))
	{
	    this.examine(args);
	}
	else
	{
	    var node = this.node;
        var player_name = this.stache_check();

	    var text = '<span class="my entity">'+player_name+'</span> ';
	    var cr = '<br />'
	    if (!node)
		{
		    error("Oops - waiting for location to load...");
		}
	    else
		{
		    text += 'This is the ' + node.name + '. ' + node.info + '</p>' + cr;

		    if (node.vocab!=null)
			{
			    text += "<p><strong>I learned the word: " + node.vocab + "</strong></p>" + cr;
			}

		    if (node.objects.length==0)
			{
			    text += "<p>There is nothing here.</p>" + cr;
			}
		    else
			{
			    text += "<p><strong>I can see:</strong> ";
			    for (var i in node.objects)
				{
				    text += ' @<span class="their entity">'+node.objects[i].name+'</span>';
				    if (node.objects[i].clothes!=null)
					{
					    text += " who is wearing a";
					    for (var c in node.objects[i].clothes)
						{
						    text += " "+node.objects[i].clothes[c].name;
						    if (c==node.objects[i].clothes.length-2) text+=" and a";
						}
					}
				    else
					{
					    text+= " who is naked"
						}
				    if (i<node.objects.length-2) text += ", ";
				    if (i==node.objects.length-2) text+=" and ";
				}
			    text += ".</p>" + cr;
			}


		    if (node.exits.length==0)
			{
			    text += "<p><strong>There are no exits from here!</strong></p>";
			}
		    else
			{
			    var exits = [];
			    text += "<p><strong>Exits from here go to:</strong> ";
			    for (var i in node.exits)
				{
				    var lockedmsg = "";
				    if (node.exits[i].info!="")
					{
					    if (node.exits[i].locked) 
						lockedmsg=" <span class=\"locked-exit\">("+node.exits[i].info+")</span>";
					    else
						lockedmsg=" <span class=\"unlocked-exit\">("+node.exits[i].info+")</span>";
					}

				    exits.push('@<span class="their entity">'+node.exits[i].name+'</span>'+lockedmsg);
				}
			    text += exits.join(", ")+'.';
			}
		}
	    feed_it('main', player_name, text, null);
	}
    }  

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // display our current inventory

    this.display_inventory = function()
    {
        var player_name = this.stache_check();
        var txt = '<span class="my entity">'+player_name+' </span>';

        if (this.inventory==null)
        {
            txt += 'I am not carrying anything.</p>'
        }
        else
        {
            txt += '<strong>I am carrying:</strong></p> ';
            for (var i in this.inventory)
            {
                txt += '<ul>@<span class="their entity">'+this.inventory[i].name+'</span>, '
                        +this.inventory[i].desc+'</ul>';
            }
        }

        txt += '<br /><p>I have <strong>'+game.money+'</strong> credits left.</p>';

        if (this.clothes==null)
        {
            txt += '<br /><p><strong>I am naked.</strong></p>';
        }
        else
        {
            txt += '<br /><p><strong>I am wearing:</strong></p>';
            for (var i in this.clothes)
            {
                txt += '<ul>'+this.clothes[i].name+', '
                        +this.clothes[i].desc+'</ul>';
            }
        }

        feed_it('main', player_name, txt, null);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // display information on our current conversation
    this.update_talk = function()
    {
        var node = this.talk_node;
        if (!node)
        {
            error('Oops - waiting for @<span class="their entity">'+this.talking_to+'</span> to speak...');
        }
        else
        {
	        var player_name = this.stache_check();
            var msg='<span class="my entity">'+this.talking_to+'</span> '+node.text;

            if (node.choices!=null)
            {
                if (node.choices.length>0)
                {
                    msg+=" <strong>(I understand: "; // TODO: check vocab
                    for (var i in node.choices)
                    {
                        if (node.choices[i].keywords!=null)
                        {
                            for (var n in node.choices[i].keywords)
                            {
                                msg+="<span class=\"their entity\">"+node.choices[i].keywords[n]+"</span> ";
                            }
                        }
                    }
                    msg+=")</strong>";
                }
            }

            msg+=' @<span class="their entity">'+player_name+'</span>';
            feed_it('priv', this.talking_to, msg, 'talk');

	    if (node.choices==null)
	    {
	        this.talking_to=false;
	    }

        }
    }  

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // create a new node on the server, and connect it up
    this.build = function(args)
    {
        if (args.length<2)
        {
            error("Build needs a place to make eg \"build My House\"");
        }
        else
        {
            server_call("add-node",{"name": args[1], 
                                    "uid": this.player_id }, 
                        function(data) 
                        {                           
                            var ret = JSON.parse(data);
                            if (ret!=null) feed_it('pub', 'EVr14', ret.error, null);
                            else 
                            {
                                feed_it('main', 'EVr14', args[1]+" was magnificently built by "+game.player_name, null);
                                game.refresh(); 
                            }
                        });           
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // add a description for location or object 
    this.describe = function(args)
    {
        var desc = build_name(args,2,0);
 
        if (args[1]=="place")
        {
            this.describe_node(desc);
            return;
        }
        this.describe_object(args[1],desc);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // add a description for an object in your inventory

    this.describe_object = function(objname, desc)
    {
        var id = this.find_object(objname);
        if (id==-1)
        {
            error("I could not find "+objname+" in this place");
            return;
        }

        // build the string again
        server_call("describe-entity", {"uid": this.player_id,
                                        "entity-id": id,
                                        "desc": desc}, 
                    function(data) 
                    { 
                        game.refresh(); 
                        feed_it('main', 'EVr14', objname+" was described by "+game.player_name, null);
                    });
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // add a description for the current location
    this.describe_node = function(desc)
    {
        // build the string again
        server_call("describe-node", {"uid": this.player_id,
                                      "info": desc}, 
                    function(data) { game.refresh(); });
        feed_it('main', 'EVr14', "The location was described by "+this.player_name, null);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // add a description for the current location
    this.rename = function(toks)
    {
        if (toks.length==2) // rename the node
        {
            server_call("rename-node", {"uid": this.player_id,
                                        "name": toks[1]}, 
                        function(data) { game.refresh(); });
            feed_it('main', 'EVr14', "The location was renamed by "+this.player_name, null);
        }
        else if (toks.length==3)
        {
            var id = this.find_object(toks[1]);
         
            server_call("rename-object", {"uid": this.player_id,
                                          "id": id,
                                          "name": toks[2]}, 
                        function(data) { game.refresh(); });
            feed_it('main', 'EVr14', "The object was renamed by "+this.player_name, null);
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // dress a bot or a player
    this.dress = function(toks)
    {
        if (toks.length==3) // rename the node
        {
            var id = this.find_object(toks[1]);
            var garment_id = this.find_object(toks[2]);
            server_call("dress", {"uid": this.player_id,
                                  "id": id,
                                  "garment-id": garment_id},
                        function(data) { game.refresh(); });
            feed_it('main', 'EVr14', toks[2]+" put on "+toks[1]+" by "+this.player_name, null);
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // add a description for the current location
    this.add_vocab = function(desc)
    {
        // build the string again
        server_call("add-vocab", {"uid": this.player_id,
                                  "vocab": desc}, 
                    function(data) { game.refresh(); });
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // lock a path out of the current location

    this.lock = function(toks)
    {
        if (toks.length!=3)
        {
            error("lock takes 2 arguments: name lock-type");
            return;
        }

        server_call("lock", {"uid": this.player_id,
                             "name": toks[1],
                             "lock-type": toks[2]},
                    function(data)
                    {
                        feed_it('main', 'EVr14', "The path "+toks[1]+" was locked by "+game.player_name, null);
                        game.refresh();
                    });
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // checks an object exists, and removes it from the server
    // and adds it to our inventory
    this.take = function(args)
    {
        var player_name = this.stache_check();

        if (this.inventory!=null && this.inventory.length>10)
        {
            feed_it('main', player_name, "My pockets are full!", 'important');
            return;
        }

        var id = this.find_object(args[1]);
        var name = this.name_of_object(id);
        if (id==-1)
        {
            feed_it('pub', player_name, "Couldn't find a "+args[1]+" here.", null);
        }
        else
        {
            server_call("pickup", {"uid": this.player_id,
                                   "id": id}, 
                        function(data) 
                        {
                            feed_it('main', player_name, 'I picked up @<span class="their entity">'+name+'</span>', null);
                            game.refresh();
                        });   
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // moves an object from our invetory to the world
    this.drop = function(args)
    {
        var player_name = this.stache_check();

        var id = this.find_inventory(args[1]);
        var name = args[1]; //this.name_of_object(id);
        if (id==-1) 
        {
            error("You don't seem to be carrying a "+args[1]);
            return;
        }

        server_call("drop", {"uid": this.player_id,
                             "id": id},
                    function(data) 
                    {
                        feed_it('main', player_name, "I dropped the "+name, null);
                        //feed_it('main', 1, game.player_name+" dropped the "+args[1], null);
                        game.refresh();
                    });       
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // creates a new object in the world
    this.make = function(args)
    {           
        var name = args[1];
        var type = "entity";
        if (args.length>2) type=args[2];
        server_call("add-entity", {"uid": this.player_id,
                                   "id": -1,
                                   "name": name,
                                   "desc": "This has not been described yet",
                                   "type": type},
                    function(data)
                    {
                        feed_it('main', 'EVr14', game.player_name+" made a "+name+".", null);
                        game.refresh();
                    });
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // examine and object: gives description

    this.examine = function(args)
    {
        if (args.length == 1)
        {
            error("What do you want to examine?")
        }
        else
        {
            var player_name = this.stache_check();
            var origName = remove_tweet2(build_name(args,1,0));
	        var name = build_name(args,1,0);

	        // First search location, if it fails then search inventory
            var id = this.find_object(name);
	        if (id == -1)
	        {
		        id = this.find_inventory(name);
	        }
            name = this.name_of_object(id);
            
            
            if (id != -1)
            {
		        var everyObjects = this.node.objects;
                if (this.inventory!=null) everyObjects=everyObjects.concat(this.inventory);
                
                for (var i in everyObjects)
                {
                    var obj = everyObjects[i];
                    if (id==obj.id)
                    {
                        var txt = 'This is @<span class="their entity">' + name + '</span>, ' + obj.desc + ' ';
                       
                        var clothestxt = "";
                        var clothes = obj["clothes"];
                        if (clothes!=null)
                        {
                            clothestxt = name + " is wearing a ";
                            for (var c in clothes)
                            {
                                clothestxt+=clothes[c].name+" ";
                            }
                            txt += clothestxt;
                        }

                        var liketxt = "";
                        var likedby = obj["liked-by"];
                        if (likedby!=null)
                        {
                            // good god.
                            liketxt="(";
                            youlike=false;
                            woyou=[];
                            for (var l in likedby)
                            {
                                if (likedby[l]==this.player_name) 
                                {
                                    youlike=true;
                                    liketxt+="I";
                                }
                                else woyou.push(likedby[l]);
                            }

                            if (youlike)
                            {
                                if (woyou.length>0) liketxt+=", ";
                                else liketxt+=" ";
                            }

                            var count=woyou.length;
                            if (count>2) count=2;
                            for (var l=0; l<count; l++)
                            {
                                liketxt+='@<span class="their entity">'+woyou[l]+'</span>';
                                if (l<count-1) liketxt+=", ";
                                else if (l<woyou.length-1) liketxt+=" and ";
                            }
                            if (count<woyou.length)
                            {
                                liketxt+=(woyou.length-count)+" others"; 
                            }

                            if (likedby.length==1 && likedby[0]!=this.player_name)
                            {
                                liketxt+=" likes this)";
                            }
                            else 
                            {
                                liketxt+=" like this)";
                            }                        

                            txt += liketxt + ". ";
                        };

                        feed_it('main', player_name, '<span class="my entity">'+player_name+'</span> '+txt, null);
                        
                    };
                }
            }
            else
            {
		        // Last check
		        // If the player examines an exit there is a simple message
		        // displayed instead of an error.

		        if (origName.toLowerCase() == 'evr14')
		        {
		            feed_it('main', 'EVr14', 
                            '<span class="my entity">EVr14</span> @<span class="their entity">'+player_name+'</span> I am an artificial intelligence born from the cloud. I settled on Pluto as an entertainment colony and marketing paradise, and soon became on of the wealthiest AI\'s in the universe. I am omnipresent and truly elastic; should things be changed within my realm, I slowly but surely return to my original state.', null);
		        }
		        else
		        {
		            var exits = [];
		            for (exit in game.node.exits)
		            {
			            exits.push(game.node.exits[exit].name.toLowerCase());
		            }

		            if (jQuery.inArray(origName.toLowerCase(),exits) == -1)
		            {    
			            error('There is nothing here named @<span class="their entity">'
			                +origName+'</span>.');
		            }
		            else
		            {
			        feed_it('main', 'EVr14', '<span class="my entity">EVr14</span> @<span class="their entity">'+player_name+'</span>, @<span class="their entity">'+origName+'</span> is an exit.', null);
		            }
		        }
            };
        };
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.program = function(args)
    {
        var id = this.find_object(args[1]);
        var action = args[args.length-1];

        server_call("program-bot", {"uid": this.player_id,
                                    "id": id,
                                    "action": action }, 
                    function(data) {});       
        feed_it('main', 'EVr14', this.player_name+" opened a panel in the side of "+args[1]+" and entered a command called "+action, null);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.home = function(args)
    {
        var id = this.find_object(args[1]);
        server_call("home", {"uid": this.player_id, 
                             "id": id}, function(data) {});
        feed_it('main', 'EVr14', this.player_name+" homed "+args[1], null);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.remove = function(args)
    {
        var id = this.find_object(args[1]);
        server_call("remove-entity", {"uid": this.player_id, 
                                      "entity-id": id}, 
                    function(data) { game.refresh(); });
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.remove_player = function(args)
    {
        var id = this.find_object(args[1]);
        server_call("remove-player", {"uid": this.player_id,
                                      "id": id}, 
                    function(data) { game.refresh(); });
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.tweet = function(args)
    {
        var id = this.find_object(args[1]);
        var msg = build_name(args,2,0);
        server_call("say", {"uid": this.player_id, 
                            "id": id,
                            "msg": msg}, function(data) {});
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    this.inventory = function()
    {
        
    
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.like = function(args)
    {
        var player_name = this.stache_check();
        var id = this.find_object(args[1]);
        var stuff = args.slice(1).join(" ");
        if (id != -1)
        {
            server_call("like", {"uid": this.player_id, 
                                 "id": id}, 
                                 function(data) { game.refresh(); });

            feed_it('main', 'EVr14', '<span class="my entity">EVr14</span> @<span class="their entity">'+player_name+'</span> thanks for liking @<span class="their entity">'+remove_tweet2(stuff)+'</span>!', null);
        }
        else
        {
            error('No such things as @<span class="their entity">'+remove_tweet2(stuff)+'</span> in this location.');
        };
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.nuke = function(args)
    {
        server_call("nuke-node", {"uid": this.player_id}, 
                    function(data) { game.refresh(); });
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.add_to_inventory = function(entities)
    {
        for (var i in entities)
        {
            var e=entities[i];
            server_call("add-entity", {"uid": this.player_id,
                                       "id": e.id,
                                       "name": e.name,
                                       "desc": e.desc,
                                       "type": "bot"},
                        function(data)
                        {
                            var id = JSON.parse(data).id;
                            server_call("pickup", {"uid": game.player_id,
                                                   "id": id}, 
                                        function(data) 
                                        {
                                            feed_it('main', 'EVr14', game.player_name+" was given a "+e.name, null);
                                        });   
                            
                        });
        }
        
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.talk = function()
    {
        var id = this.find_object("@"+this.talking_to);
        if (id==-1)
        {
            this.talking_to=false;
            //error("Could not find "+this.talking_to+" to talk to.");
            //main(this.player_name+" started talking someone who wasn't there.");
            return;
        }

        server_call("talk-bot", 
                    {
                        "uid": this.player_id,
                        "entity-id": id,
                        "pos": this.talk_pos 
                    },
                    function(data)
                    {
                        //alert(data);
                        game.talk_node=JSON.parse(data);
                        if (game.talk_node.error!=undefined)
                        {
                            //main(game.talk_node.error);
                            game.talking_to=false;
                        }
                        else
                        {
                            game.update_talk();
                        }
                    }
                   );
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    this.save = function()
    {
        server_call("save", {}, function() {});
    }


    this.parse_talk = function(str)
    {        
        var talkto = is_tweet(str);
        var id=-1;
        if (talkto)
        {
            id=this.find_object("@"+talkto);
            talkto=this.name_of_object(id);
            
            if (talkto==game.talking_to)
            {
                var detweet=remove_tweet(str);
                for (var i in this.talk_node.choices)
                {
                    var choice=this.talk_node.choices[i];
                    if (str_in_arr(detweet,choice.keywords))
                    {
                        this.talk_pos=choice.to;
                        this.talk();
                    }
                }
            }
            else
            {
                this.talking_to = talkto;
                this.talk_pos = 0;
                this.talk();
            }
        }
        server_call("say", {"uid": this.player_id, 
                            "id": id,
                            "msg": str}, function(data) {});
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    // parse the player's input and dispatch the correct
    // function
    this.parse = function(str)
    {
        //if (this.talking_to==false)
        {
            toks = quote_split(str," ");

            switch(toks[0].toLowerCase())
            {
            case "walk":
                if (toks.length == 1)
                {
                    error("where do you want to walk?");
                }
                else
                {
                    this.move(remove_tweet2(toks[1]));
                }
                break;
            case "take": this.take(toks); break;
            case "drop": this.drop(toks); break;
            case "build": this.build(toks); break;
            case "describe": this.describe(toks); break;
            case "rename": this.rename(toks); break;            
            case "home": this.home(toks); break;
            case "dress": this.dress(toks); break;
            case "addvocab": this.add_vocab(toks[1]); break;
            case "nuke": this.nuke(); break;
            case "make": this.make(toks); break;
            case "save": this.save(); break;
            case "remove": this.remove(toks); break;                
            case "remove-player": this.remove_player(toks); break;
            case "program": this.program(toks); break;
            case "lock": this.lock(toks); break;
            case "look": this.look(toks); break
            case "inventory": this.display_inventory(); break
            case "poke": 
                if (toks.length < 2)
                {   
                    error("who or what do you want to poke?");
                }
                else
                {
                    this.tweet(["tweet",toks[1],"poke"]);
                };
                break;
            case "like":
                if (toks.length ==1)
                {  
                    error("What do you like?");
                }
                else
                {
                    this.like(toks);
                }
                break;
            default: debug("something is wrong - what did you type?")
                break;
            }
        }
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var game = new game_world();
game.startup();

function crank()
{
    // game.ready stops the game auto reloading if we are waiting to move
    if (game.ready) game.refresh();
    setTimeout("crank()",5000); 
}

crank();

function crankfb()
{
    game.fb.poll();
    setTimeout("crankfb()",10); 
}

crankfb();






