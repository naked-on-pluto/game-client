// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

server_call("get-votes", {}, 
            function(data) 
            {
                //alert(data);
                var v = JSON.parse(data);
                if (v[0]!=null)
                {
                    for (i in v[0])
                    {
                        $('#'+"for").append('<div id="'+"for"+'-new">'+v[0][i]+'<br></br></div>');
                    }
                }
                if (v[1]!=null)
                {
                    for (i in v[0])
                    {
                        $('#'+"against").append('<div id="'+"against"+'-new">'+v[1][i]+'<br></br></div>');
                    }
                }
            });

