/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function stitch(start,end,from_pos,to_pos,from_dir,to_dir)
{
    this.start=start;
    this.end=end;
    this.from_pos=from_pos;
    this.to_pos=to_pos;
    this.from_dir=from_dir;
    this.to_dir=to_dir;

    this.eq = function(other)
    {
        return (this.start==other.start || this.start==other.end) &&
            (this.end==other.end || this.end==other.start); 
    }
}
        
function library()
{
    this.lib_nodes = {};
    this.factory=new brx_factory();
    this.next_location=0;
    this.stitches = [];
    this.obj_tracker = new obj_tracker();
    this.listener = new listener();
    this.node_map={};
    this.reset_time=0;
    this.reset_period=30000;
    this.strapline="LIVE GAME WORLD EXPOSURE";
    this.strapwidth=0;

    // nodes we wish to update
    this.node_names=[
        "PalaceEntrance",
        "Helpdesk",
        "PalaceGarden",
        "ArrivalLobby",
        "GameRoom",
        "ProfileRoom",
        "DanceFloor",
        "TVStudio",
        "GameRoom",
        "ProfileDeactivationChamber",
        "SheepField",
        "Toilets",
        "UpperHighStreet",
        "PonyGiftShop",
        "Boudoir",
        "MafiaRoom",
        "Bar",
        "Square"
    ]

    this.init = function()
    {
        this.lib_nodes = {};
        this.next_location=0;
        this.stitches = [];
        this.node_map={};

        this.obj_tracker.clear();

        var num_nodes=8;
        var nodes=[];
        while (nodes.length<num_nodes)
        {
            var name=choose(this.node_names);
            if (!contains(nodes,name))
            {
                nodes.push(name);
            }
        }

        nodes.push("Library");

        // build the node map
        var a=0;
        var tot=(3.141*2)/nodes.length;
        for (var i in nodes)
        {
            this.node_map[nodes[i]]=
                [library_centre_x+Math.sin(a)*200,
                 library_centre_y+Math.cos(a)*200,
                 a];
            a+=tot;
        }
    }

    // helper to tidy away server impl/jquery gumpf
    this.load_node = function(name,f)
    {
        $.get("main", {function_name:"get-node-library",
                       name:name},f);
    }
    
    // called periodically to get stuff from server
    this.update = function()
    {
        var location_num=this.next_location;
        var location_name=object_keys(this.node_map)[location_num];
        
        var that=this;

        this.load_node(location_name,function(data) {
            var src=JSON.parse(data);

            if (that.lib_nodes[location_name]==null)
            {
                var new_node=
                    new lib_node(location_name,
                                 src, that.factory,
                                 that.node_map[location_name][2]);
                that.lib_nodes[location_name]=new_node;
                    
                that.update_stitches();
            }
            else
            {
                var node=that.lib_nodes[location_name];
                node.update(src);
                that.obj_tracker.update(node,
                                        that.node_map,
                                        that.factory);

                that.listener.spybot_check(src);

                if (location_name=="Library")
                {
                    that.listener.update(src.messages);
                }
            }
        });
        
        // loop around locations updating them
        this.next_location=(this.next_location+1)%
            object_size(this.node_map);
    }

    // loop through nodes, drawing them in the right 
    // positions 
    this.draw_nodes = function(ctx)
    {
        this.draw_stitch(ctx);
        for (var name in this.node_map)
        {
            var pos=this.node_map[name];
            if (this.lib_nodes[name]!=null)
            {
                this.lib_nodes[name].draw(ctx,pos[0],pos[1]);
            }
        }
    }

    // look for stitches in the cache
    this.search_stitches = function(s)
    {
        for (var i in this.stitches)
        {
            if (this.stitches[i].eq(s)) return true;
        }
        return false;
    }

    // update stitches from nodes
    this.update_stitches = function(ctx)
    {
        for (var name in this.lib_nodes)
        {
            var pos=this.node_map[name];
            for (var other in this.lib_nodes)
            {
                var other_pos=this.node_map[other];
                var one=this.lib_nodes[name];
                var two=this.lib_nodes[other];

                if (one.do_exits_contain(other))
                {
                    var onevec=[-Math.sin(one.angle)*100,
                                -Math.cos(one.angle)*100];
                    var twovec=[-Math.sin(two.angle)*100,
                                -Math.cos(two.angle)*100];

                    var from_dir = vadd(pos,onevec);
                    var to_dir = vadd(other_pos,twovec);
                    
                    var s=new stitch(name,other,pos,other_pos,from_dir,to_dir);
                    if (!this.search_stitches(s))    
                    {
                        this.stitches.push(s);
                    }
                }
            }
        }
    }

    this.draw_stitch = function(ctx)
    {
        for (var i in this.stitches)
        {
            var s=this.stitches[i];
            ctx.beginPath();
            ctx.lineWidth=1
            ctx.moveTo(s.from_pos[0],s.from_pos[1]);
            ctx.bezierCurveTo(s.from_dir[0], s.from_dir[1], 
                              s.to_dir[0], s.to_dir[1], 
                              s.to_pos[0], s.to_pos[1]);  
            ctx.stroke();  
            ctx.lineWidth=1;
         }
    }

    this.image_ready=false;
    this.image = new Image();
    this.image.onload = function(){
        this.image_ready=true;
    };
    this.image.src = "logo.png";

    // called every frame
    this.draw = function() 
    {  
        var ctx = document.getElementById('canvas').getContext('2d');         
        var time = new Date();  
        var t = ((2*Math.PI)/60)*time.getSeconds() + 
            ((2*Math.PI)/60000)*time.getMilliseconds();
        

        //    ctx.globalCompositeOperation = 'destination-over';  
        ctx.clearRect(0,0,screen_width,screen_height); // clear canvas  
        ctx.setTransform(1,0,0,1,0,0);

	// ctx.scale(0.7,0.7);
        
        ctx.fillStyle = fg_colour;
        ctx.strokeStyle = fg_colour;  
        ctx.font = "bold 9pt Courier";
        ctx.save();  

        ctx.drawImage(this.image, 
                      library_centre_x-23,
                      library_centre_y-50);
        ctx.globalCompositeOperation = 'source-atop';
        ctx.fillRect(library_centre_x-23,
                     library_centre_y-50,
                     library_centre_x+23,
                     library_centre_y+50);
        ctx.globalCompositeOperation='source-over';

        if (Math.floor(t*10)%2==0)
        {
            if (this.strapwidth==0)
                this.strapwidth=ctx.measureText(this.strapline).width;
            
            ctx.fillText(this.strapline, 
                         library_centre_x-this.strapwidth/2,
                         library_centre_y+55);
        }
	/*
        ctx.beginPath();
        ctx.arc(library_centre_x,library_centre_y,
                library_centre_x,0,Math.PI*2,true);
        ctx.closePath();
        ctx.stroke();
	*/
/*
        ctx.beginPath();
        ctx.arc(library_centre_x,library_centre_y,
                (1+Math.sin(t*10))*5,0,Math.PI*2,true);
        ctx.closePath();
        ctx.stroke();
*/ 
        this.listener.draw(ctx);
        this.draw_nodes(ctx);
        this.obj_tracker.draw(ctx);
/*
        ctx.globalCompositeOperation='destination-atop';
        ctx.fillStyle = bg_colour;

        ctx.beginPath();
        ctx.arc(library_centre_x,library_centre_y,
                library_centre_x,0,Math.PI*2,true);
        ctx.closePath();
        ctx.fill();

        ctx.globalCompositeOperation='source-over';
*/

        ctx.restore();      

        this.reset_time--;

        if (this.reset_time<0)
        {
            this.init();
            this.reset_time=this.reset_period;
        }

    }  

}

////////////////////////////////////////////

var library = new library();
function draw() { library.draw(); }
function update() { library.update(); }

library.init();

function init()
{   
    //canvas.addEventListener('mousedown', ev_canvas, false);
    //canvas.addEventListener('mousemove', ev_canvas, false);
    //canvas.addEventListener('mouseup',   ev_canvas, false);
    //canvas.addEventListener('mousemove', mouse_move, false);
    setInterval(draw,50);  
    setInterval(update,1000);  
}  


