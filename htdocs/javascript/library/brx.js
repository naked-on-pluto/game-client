/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var screen_width=768;
var screen_height=768;
var library_centre_x=screen_width/2;
var library_centre_y=screen_height/2;
var fg_colour="rgba(64,64,255,1)";
var bg_colour="rgba(0,0,0,1)";

var brx_height=12;
var brx_width=50;
var brx_text_adj_x=2;
var brx_text_adj_y=-2;

function brx_atom(name,is_entity)
{
    this.name=name;
    this.age=0;
    this.speed=0.6;
    this.is_entity=is_entity;
    this.fg_colour=fg_colour;
    this.bg_colour=bg_colour;

    if (is_entity)
    {
        this.fg_colour=bg_colour;
        this.bg_colour=fg_colour;
    }

    this.draw=function(ctx, x, y)
    {
        var text=this.name;
        if (text.length>this.age)
        {
            text=text.substring(0,this.age);
        }

        ctx.fillStyle = this.fg_colour;  

        var m=ctx.measureText(text);
        var w=m.width+5;
        var h=brx_height;

        ctx.beginPath();
        ctx.moveTo(x,y-h);
        ctx.lineTo(x+w,y-h);
        ctx.lineTo(x+w,y);
        ctx.lineTo(x,y);
        ctx.closePath();
        ctx.fill();
        ctx.strokeStyle = this.bg_colour;  
        ctx.stroke();

        ctx.fillStyle = this.bg_colour;  

        ctx.fillText(text, 
                     x+brx_text_adj_x, 
                     y+brx_text_adj_y);
        this.age+=this.speed;
        return y+h;
    }
    
    this.start_decay=function()
    {
        this.age=100;
        this.speed=-1;
    }
}


// takes a list of brx_atoms or other brx_lists
function brx_list(name,items,is_entity)
{
    this.name=name;
    this.items=items;
    this.age=0;
    this.speed=0.6;
    this.is_entity=is_entity;
    this.fg_colour=fg_colour;
    this.bg_colour=bg_colour;

    if (is_entity)
    {
        this.fg_colour=bg_colour;
        this.bg_colour=fg_colour;
    }

    this.draw=function(ctx, x, y)
    {        
        var sx=x;
        var sy=y;
        var text=this.name;
        if (text.length>this.age)
        {
            text=text.substring(0,this.age);
        }

        var textx=x+brx_text_adj_x;
        var texty=y+brx_text_adj_y;

        var m=ctx.measureText(text);
        var w=m.width+5;
        var h=m.height+2;

        x+=brx_height;
        y+=brx_height;
        
        var count=this.age*0.1;
        if (this.items.length<count) count=this.items.length;
        
        for (var i=0; i<count; i++)
        {
            y=this.items[i].draw(ctx,x,y);
        }

        ctx.fillStyle = this.fg_colour;  

        ctx.beginPath();
        ctx.moveTo(sx,sy-brx_height);
        ctx.lineTo(sx+w,sy-brx_height);
        ctx.lineTo(sx+w,sy);
        ctx.lineTo(sx+brx_height,sy);
        ctx.lineTo(x,y-brx_height);
        ctx.lineTo(x+w-brx_height,y-brx_height);
        ctx.lineTo(x+w-brx_height,y);
        ctx.lineTo(x-brx_height,y);
        ctx.closePath();
        ctx.strokeStyle = this.bg_colour;  
        ctx.fill();
        ctx.stroke();

        ctx.fillStyle = this.bg_colour;  

        ctx.fillText(text,textx,texty);

        this.age+=this.speed;

        return y+brx_height;
    }

    this.start_decay=function()
    {
        this.age=100;
        this.speed=-2;
        for (var i in this.items)
        {
            this.items[i].start_decay();
        }
    }

}

function brx_factory()
{
    this.build=function(name,obj,is_entity)
    {
        var ret=[];
        for (var i in obj)
        {
            if (typeof(obj[i])=="object")
            {
                ret.push(this.build(i,obj[i],is_entity));
            }
            else
            {
                ret.push(new brx_atom(obj[i],is_entity));
            }
        }
        return new brx_list(name,ret,is_entity);
    }

    this.build_update=function(name,obj)
    {
        var ret=[];
        for (var i in obj)
        {
            if (typeof(obj[i])=="object")
            {
                ret.push(this.build_update(i,obj[i]));
            }
            else
            {
                var at=new brx_atom(obj[i]);
                at.age=9999;
                ret.push(at);
            }
        }
        var ret=new brx_list(name,ret);
        ret.age=9999;
        return ret;
    }
}
