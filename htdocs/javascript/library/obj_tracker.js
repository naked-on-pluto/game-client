/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function animated_obj(from_pos,to_pos,from_a,to_a,src,factory)
{
    this.t=0;
    this.from_pos=from_pos;
    this.to_pos=to_pos;
    this.from_a=from_a;
    this.to_a=to_a;
    this.src=src;
    this.filter=new obj_filter();
    this.brx=factory.build(src.name,this.filter.filter(src),true);
    this.decay=false;

    this.image_ready=false;
/*   this.image = new Image();
    this.image.onload = function(){
        this.image_ready=true;
    };
    this.image.src = "http://evr14.naked-on-pluto.net/images/avatar/SpyBot001.png";
*/
    this.draw = function(ctx)
    {
        var x=this.from_pos[0]*(1-this.t)+this.to_pos[0]*this.t;
        var y=this.from_pos[1]*(1-this.t)+this.to_pos[1]*this.t;

        ctx.save();
        ctx.translate(x,y);


        var fa=this.from_a;
        var ta=this.to_a;
        ctx.rotate(-(fa*(1-this.t)+ta*this.t));
        ctx.translate(-x,-y);

        if (this.image_ready==true)
        {
            ctx.drawImage(this.image, x, y-50);
        }

        this.brx.draw(ctx,x-brx_width/2,y+brx_height);
        ctx.restore();

        if (this.t>0.75 && this.decay==false)
        {
            this.brx.start_decay();
            this.decay=true;
        }

        this.t=this.t+0.005;
    }
}

function obj_tracker()
{
    this.missing_objs=[];
    this.animated_objs=[];

    this.clear = function()
    {
        this.missing_objs=[];
        this.animated_objs=[];
    }

    this.is_missing = function(obj)
    {
        for (var i in this.missing_objs)
        {
            if (this.missing_objs[i].obj.id==obj.id) 
            {
                // remove it
                var t=this.missing_objs[i];
                this.missing_objs.splice(i,1);
                return t;
            }
        }
        return false;
    }
    
    // to be called on each node after update
    this.update = function(lib_node,node_map,factory)
    {
        // first collect missing objs
        for (i in lib_node.objects_gone)
        {
            this.missing_objs.push(
                {name:lib_node.name,
                 angle:lib_node.angle,
                 obj:lib_node.objects_gone[i]});
        }

        // then check newly arrived ones with list
        for (i in lib_node.objects_arrived)
        {
            var obj=lib_node.objects_arrived[i];
            // if we find it, add it to the list
            var missing=this.is_missing(obj);
            if (missing && missing.name!=lib_node.name &&
                this.animated_objs.length<3)
            {
                this.animated_objs.push(
                    new animated_obj(
                        node_map[missing.name],
                        node_map[lib_node.name],
                        missing.angle,
                        lib_node.angle,
                        obj,
                        factory));
            }           
        }
    }

    this.draw = function(ctx)
    {
        for (var i in this.animated_objs)
        {
            var obj=this.animated_objs[i];
            obj.draw(ctx);
            if (obj.t>1)
            {
                this.animated_objs.splice(i,1);
            }
        }
    }
}