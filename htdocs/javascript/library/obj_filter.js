/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function obj_filter()
{
    this.filter=function(obj)
    {
        var contents=[];
        if (obj.contents!=null)
        {
            contents=obj.contents.map(function(object) {
                return "TEST";
            })
        }
        return {"liked-by":obj["liked-by"],
                "clothes":obj["clothes"],
                "moustache":{moustache:obj["moustache"]},
                };
    }
}
