// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// code to provide the "readme" sliding popup on the site
// It can be commented out from index.html or repurposed when we release NOP

function help_page(page)
{
        var textA = '<br /><b>Welcome to the EVr14 manual!</b><br />You look fantastic today.<br /><br />' +
                    'This handcrafted manual is made of four tiny magnificent organic pages, use the number menu to navigate through this delighful eco friendly document.<br /><br />' +
                    'EVr14 automatically subscribed you to <b>three different feeds</b> taylored to your very special needs and interests:' +
                    '<ul><li><b>Mass Syndication</b> (right): a general message feed that displays all the messages sent in a room,</li>' +
                    '<li><b>Self Syndication</b> (center): to keep track of your actions with EVr14,</li>' +
                    '<li><b>Targetted Syndication</b> (left): a personal feed that displays special messages directed at you. It is not a private feed. We know you have nothing to hide!</li></ul>';

        var textB = '<br /><b>Everything you type is posted to the Mass Syndication feed</b>.' +
                    ' If you want to target your message to a bot or person, you can use the "@" sign followed by the name of the bot or person <em>(i.e. <b>@Bob How are you</b>?).</em><br /><br />' +
                    'When you start you also have access to a subset of special commands, such as:' +
                    '<ul><li><b>look (1)</b> - <i>look</i> - just typing <em><b>look</b></em> gives you a description of the space you are in, who and what is in it, and which exits are there.</li>' +
                    '<li><b>look (2)</b> - <i>look something/someone (i.e. <b>look @Gamebot2000</b>)</i> - gives you a description of an object, bot or person.</li>' +
                    '<li><b>walk</b> - <i>walk somewhere</i> - you can navigate from one location to another with this command <i>(i.e. <b>walk @ArrivalLobby</b>)</i>.</li>' +
                    '<li><b>inventory</b> - <i>inventory</i> - gives you a list of all the items you are carrying with you.</li></ul>';

        var textC = '<br /><b>As your progress in your pleasant journey, you will have access to more commands</b>. Watch carefully the feeds as the availability of a new command will be announced on these. In total you can learn up to four more commands:' +
                    '<ul><li><b>like</b> - <i>like something/someone (i.e. <b>like @GlitterBall</b>)</i>.</li>' +
                    '<li><b>poke</b> - <i>poke something/someone (i.e. <b>poke @Bob</b>)</i>.</li>' +
                    '<li><b>take</b> - <i>take something/someone</i> - and place it in your inventory <i>(i.e. <b>take @Apple</b>)</i>.</li>' +
                    '<li><b>drop</b> - <i>drop something/someone</i> - from your inventory, at your current location <i>(i.e. <b>drop @Pony</b>)</i>.</li></ul>';

        var textD = '<br />To make your interaction with EVr14 a grand experience of finely tuned consumer delicatesse we are also providing the following <b>free mandatory opt-out-not services</b>:' +
                    '<ul><li><b>Up/Down Arrows</b> - You can use the up arrow to go back to what you previously typed.</li>' +
                    '<li><b>Auto Save</b> - Your current location and inventory are automatically saved and stored up to 7 days in the magical pony cloud, so you can safely logout and resume your customer experience later on.</li>' +
                    '<li><b>@ links</b> - Most @ names are clickable and will complete or pre-fill the input form for faster completion.</li></ul>' +
                    '<br /><br /><b>Have a fantastic time with us!</b>';

//                    '<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fis.so.convenient&amp;width=292&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;header=true&amp;height=62" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:62px;" allowTransparency="true"></iframe>';

        $('#help-page').empty();

        if (page === 1)
        {
            $('#help-page').append(textA);
        }
        else if (page === 2)
        {
            $('#help-page').append(textB);
        }
        else if (page === 3)
        {
            $('#help-page').append(textC);
        }
        else if (page === 4)
        {
            $('#help-page').append(textD);
        }
}

$(document).ready(function () 
{
    help_page(1);

    var popup = $("#readme");
    var content = popup.children("#readme-content");
    var img = content.children("img");
            
    popup.css("display", "block").data("showing", false);
    
    // cursor update
    img.mouseover(function ()
    {
        $(this).css('cursor', 'pointer');
    });
           
    // slide in and out...
    img.click(function ()
    {
        if (popup.data("showing") === true)
        {
            popup.data("showing", false).animate(
            {
                marginLeft: "-655px"
            }, 500);
            $(this).attr("src", "images/help.png").css("top", "0px");
        } 
        else
        {     
            popup.data("showing", true).animate(
            {
                marginLeft: "0"
            }, 500);
            $(this).attr("src", "images/close.png").css("top", "0px");
        }
    });

});

