// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//// utils //////////////////////////////////////////////////////////////////

// returns a random element from the array
function choose(arr)
{
    return arr[Math.floor(Math.random()*arr.length)];
}

// howd ya like them white spaces?
function ws(amount)
{
    var r="";
    for (var i=0; i<amount; i++)
    {
        r+=" ";
    }
    return r;
}

// Random color generator
function random_color() {
    var chars = '0123456789ABCDEF'.split('');
    var color = '#'
    for (var i = 0; i < 6; i++ ) {
        color += choose(chars);
    }
    return color;
}

//center a div
jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + "px");
    this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
    return this;
}


// send something to inspector's or firebug's console if debug mode
// is enabled and if the browser understands console.log()
if (typeof console == "undefined")
{
    var console = { log: function() {} };
}
function debug(something)
{
    if (naked_on_pluto_debug == true)
    {
        console.log("DEBUG: " + something);
    }
}

// converts a js object into text for displaying
function object_to_string(obj,depth)
{
    text="";
    for (i in obj)
    {
        if (typeof(obj[i])=="object")
        {           
            text+=i+":\n"+object_to_string(obj[i],depth+1);
        }      
        else
        {
            text+=i+":"+obj[i]+"\n";
        }
    }
    return text;
}

// set cursor to the end of form
(function($)
{
    jQuery.fn.putCursorAtEnd = function()
    {
        return this.each(function()
        {
            $(this).focus()
            // If this function exists...
            if (this.setSelectionRange)
            {
                // ... then use it
                // (Doesn't work in IE)
                // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                var len = $(this).val().length * 2;
                this.setSelectionRange(len, len);
            }
            else
            {
                // ... otherwise replace the contents with itself
                // (Doesn't work in Google Chrome)
                $(this).val($(this).val());
            }
            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;
        });
    };
})(jQuery);


// Clean all the feeds
function clean_feeds()
{
    $('#priv').empty();
    $('#main').empty();
    $('#pub').empty();
}



//// Increment tweet times //////////////////////////////////////////////////

$(document).ready(function()
{
    window.setInterval(function()
    {
	$('#wrapper-game .time').each(function()
	{
	    var tweetTime = parseInt($(this).text().split(' ')[0]);
	    $(this).empty().append((tweetTime+1)+' minutes');
	});
    }, 60000);
});

//// add HTML inside ... ////////////////////////////////////////////////////

// .... any feeds
function feed_it(target, id, txt, flag)
{
    var localAvatars = ['AdBot', 'AdverBot', 'Barman', 'CleanerBot', 
    'Cleaner', 'Cow001', 'Cow002', 'Cow003', 'Cow004', 'Cow005',
    'Cow006', 'Cow007', 'CustomerExperienceManager', 'CustomerHelper', 
    'DogBot', 'DoorBot', 'EVr14', 'GameAssistant', 'GameBot2000', 
    'GangsterBot', 'HairLifestyleBot', 'HazelBuzzBot', 'LoveBot',
    'LurveBot', 'MafiosiDroid', 'MarketingBot', 'MoustachBot', 
    'PokerBot', 'PoolCleanerBot', 'Sheep001', 'Sheep002', 'Sheep003',
    'Sheep004', 'Sheep005', 'Sheep006', 'Sheep007', 'Sheep008', 
    'Sheep009', 'Sheep010', 'Sheep011', 'SpandexDanceBot001', 
    'SpandexDanceBot002', 'SpandexDanceBot003', 'SpringleFooBot', 
    'UndefinedMoustacheBot', 'WelcomeBot', 'YeeHrBot', 'CustomerProcessingBot',
    'OldHorse', 'PlasticFish', 'Statue', 'AdvertBot', 'Librarian001',
    'Librarian002', 'Librarian003', 'Librarian004', 'Librarian005',
    'UndefinedPerson', 'InterviewBot', 'ReporterBot001',
    'ReporterBot002', 'ReporterBot003', 'AudienceBot001', 'AudienceBot002', 'AudienceBot003', 
    'AudienceBot004', 'AudienceBot005', 'SpyBot001', 'SpyBot002', 'SpyBot003', 'SpyBot004', 'SpyBot005'];
    var avatar = '';

    if (localAvatars.indexOf(id) == -1)
    {
        if (id.indexOf('person'))
        {
            id = game.fb.substitute_fake_info(id);
        }

        avatar = '<img src="http://graph.facebook.com/'+game.uid_of_name(id)+'/picture" />';
    }
    else
    {
        avatar = '<img src="images/avatar/'+id+'.png" />';
    }

    var html = '<table><tr><th class="face">'+avatar+'</th>'
                +'<th class="'+flag+'"><p>'
	        +game.fb.substitute_fake_info(txt)
                +'<span class="tweet-meta"><span class="time">1 minute</span> ago via EVr14 at '
	        +game.node.name+ '</span></p></th></tr></table>'

    $('.'+target+'-new').attr('class',target+'-old');
    $('.'+target+'-head').css("opacity", "0");
    $('#'+target).prepend('<div class="'+target+'-new">'+html+'</div>');
    $('.'+target+'-new').css("opacity", "0");

    $('.'+target+'-new').animate({opacity: 1});
    $('.'+target+'-head').animate({opacity: 1});

    // limit the feed size to avoid slowdown
    while ($('.'+target+'-old').length > 66)
    {
        $('.'+target+'-old:last-child').remove();
    };

}

// ... the error box
function error(html)
{
    var player_name = game.stache_check();

    feed_it('main', 'EVr14', 
        '<span class="my entity">EVr14</span> @<span class="their entity">'
        + player_name + '</span> ' +html, null);
}

// ... the facebook status box
function facebook_status(html) {$("#fb-status").empty().append(html);}




// the loader keywords //////////////////////////////////////////////////////
function newLoaderKeyword()
{
    // Marketting lingo! FTW!
    var keywords = ['Accordion Insert', 'Ad Copy', 'Advertising Allowance',
    'Advertising Budget', 'Advertising Elasticity', 'Advertising Plan',
    'Advertising Research', 'Ad Specialty', 'Advertorial', 'Affiliate', 
    'Affiliate Marketing', 'Affiliate Directory', 'Affiliate Network', 
    'Agency Commission', 'Banner Ad', 'Brand', 'Brand Identity', 'Brand Image',
    'Brand Manager', 'Business-to-Business Advertising', 'Caption', 'Car Card',
    'Card Rate', 'Channel Distribution', 'Circulation', 
    'Classified Advertising', 'Comparative Advertising', 
    'Competition-Oriented Pricing', 'Cooperative Advertising', 'Copyright',
    'Corrective Advertising', 'Cost Efficiency', 'Cost Per Inquiry',
    'Cost Per Rating Point (CPP)', 'Cost Per Thousand (CPM)',
    'Counter Advertising', 'Creative Strategy', 'Creatives', 'DAGMAR',
    'Day-After Recall Test', 'Deceptive Advertising', 'Demographics',
    'Direct Mail', 'Direct Marketing', 'Direct Response', 'Earned Rate', 
    'Eighty-Twenty Rule', 'End-User', 'Equal Time', 'Exposure', 'Eye Tracking',
    'FCC', 'Fixed-Sum-Per-Unit Method', 'Flat Rate', 'Flighting',
    'Focus Group Interview', 'Four Ps', 'FTC', 'Full Position',
    'Full-Service Agency', 'Galvanometer Test', 'Generic Brand',
    'Gross Audience', 'Gross Impressions', 'Gross Rating Points (GRPs)',
    'Hierarchy-Of-Effects Theory', 'Holding Power', 'Holdover Audience',
    'Horizontal Discount', 'Horizontal Publications', 'House Agency', 
    'Image Advertising', 'In-Pack Premium', 'Industrial Advertising',
    'Infomercial', 'Integrated Marketing Communication (IMC)', 'Jingle',
    'Jumble Display', 'Keeper', 'Key Success Factors', 'Leave-Behind',
    'Lifestyle Segmentation', 'List Broker', 'Loss Leader', 'Loyalty Index',
    'Macromarketing', 'Marginal Analysis', 'Market Profile',
    'Market Segmentation', 'Market Share', 'Marketing Firm', 'Marketing Mix',
    'Marketing Research', 'Materiality', 'Media Strategy',
    'Motivation Research', 'NAD', 'Narrowcasting', 'National Brand',
    'Net Unduplicated Audience', 'Nominal Scale', 'Nonprofit Marketing',
    'Norms', 'Objectives', 'Observation', 'On-pack (On-pack Premium)',
    'Parity Products', 'Patronage Motives', 'Payment Threshold',
    'Penetrated Market', 'Per Inquiry', 'Perceived Risk',
    'Percent-of-Sales Method', 'Persuasion Process', 'Potential Market',
    'Product Differentiation', 'Product Life Cycle', 'Product Positioning',
    'Promotion', 'Promotional Mix', 'Psychological Segmentation',
    'Psychographics', 'Publicity', 'Pupilometrics', 'Qualitative Research',
    'Quality Control', 'Quantitative Research', 'Questionnaire', 'Range',
    'Rate Card', 'Reach', 'Reference Group', 'Referral Premium',
    'Selling Orientation', 'Slogan', 'Strategic Market Planning',
    'Subliminal Perception', 'Tag line', 'Target Audience', 'Target Market',
    'Target Market Identification', 'Unique Selling Proposition',
    'Values and Lifestyles (VALS) Research', 'Vehicle', 'Vertical Publications',
    'Vision', 'Word of Mouth Advertising', 'Wear Out', 'Word Painting'];

    var rcolr = random_color();

    $('html').css('background-color', rcolr);
    $('#loader').css('text-shadow', '10px 10px'+rcolr);

    $('#loader-quantity').empty();
    $('#loader-quantity').append(Math.floor(Math.random()*99+1));
    
    $('#loader-keyword').empty();
    $('#loader-keyword').append(choose(keywords));

    $('#loader').center();
}



//// Super Naked Special Effects (TM) ///////////////////////////////////////

$(document).ready(function()
{
    window.setInterval(function()
    {
        if ($('#wrapper-game').css('display') != 'none')
        {
            if (game.stache_check() == "UndefinedPerson")
            {   
                var colors = ['#F00','#0F0','#00F'];
                var glitch = [1,2,3]; 

                $('html').css('background-color', choose(colors));
                $('html').css('background-image', 'url(images/pluto_logo_glitch'+choose(glitch)+'.png)');
                $('html').css('background-position', 'center center');
                $('html').css('background-repeat', 'repeat');
            }
            else if (game.stache_check() == "#person#13#")
            {
                $('html').css('background-image', 'url(images/theatrical_opening-glitch.jpg)');
                $('html').css('background-position', 'center center');
                $('html').css('background-repeat', 'repeat');
            }
            else
            {      
                //$('body').css('background-image', 'url(images/body-gradient.png)');
                $('html').css('background-image', 'url(images/theatrical_opening.jpg)');
                $('html').css('background-color', '#6D84B4');
                $('html').css('background-position', 'center bottom');
            }
        }
    }, 125);
});


//// fonts size and scaling /////////////////////////////////////////////////

// compute font pixel size based on an give page ratio
// used for testing purposes, see font_size_refresh() 
// for final font scale settings
function scale_font(id, ratio)
{
    newFontSize = $(window).height() * ratio;
    $(id).css('font-size', newFontSize);
}

// resize all the fonts based on hardcoded scales
// and current viewport height
function font_size_refresh()
{
    var fontRatios = {  '#fb-status': 0.02,
                        '#story':0.042,
                        '#input2': 0.03,
                        '#error':0.025,
                        '#info':0.02,
                        '#whatever':0.02 };

    
    var viewportHeight = $(window).height();

    $.each(fontRatios, function(id, ratio)
    {
        newFontSize = Math.round(viewportHeight * ratio);
        $(id).css('font-size', newFontSize + 'px');
        $(id).css('line-height', newFontSize + 'px');
    });
}

