// facebook api stuff

function fb_interface(appid,refresh)
{
    FB.init({appId: appid, status: true, cookie: true, xfbml: true});

    this.refresh=refresh;

    this.data = {
        me: {},
        people: [],
        books: [],
	    movies: [],
        places: []
    }

    /////////////////////////////////////////////////////////////////
    // the login stuff

    // check to see if the user has logged in, and ask for login if they haven't
    this.init=function()
    {
        var fb=this;
        facebook_status("connecting with facebook...");
        FB.getLoginStatus(function(response) {
            if (response.session) {
                facebook_status("You are now naked on Pluto.");
                // we are already logged in
                fb.suck();
            } else {
                // the login is a popup so need the user to click on a button
                // for the browser to allow this to happen
                facebook_status('You need to Login :<input type="button" value="Login to facebook" onClick="login();">');
            }
        });
    }

    // do the login onto facebook
    this.login=function()
    {
        FB.login(function(response) {
            if (response.session) {
                facebook_status("You are now naked on Pluto.");
                this.suck();
            } else {
                facebook_status(":( the login didn't work! " + '<input type="button" value="Login to facebook" onClick="login();">');
            }
        });
    }

    ////////////////////////////////////////////////////////////////
    // get the data from the api

    this.safe_add = function(whence,data)
    { 
	    if (data!=undefined) 
        {
            debug("adding "+data+" to "+whence);
            this.data[whence]=data;
        }
    }

    this.suck_things=function(things,data)
    {
	var fb=this;
        //debug(object_to_string(things));
	    $.each(things,function(index,thing){
           // debug(thing.category);
		    if (thing.category=="Books") fb.safe_add("books",thing.name);
		    if (thing.category=="Movie" || thing.category=="Film") 
			fb.safe_add("movies",thing.name);		
	    });
    }

    this.suck_from_friend = function(friend,data)
    {
	    if (friend.location!=undefined) this.safe_add("locations",friend.location.name);
        if (friend.hometown!=undefined) this.safe_add("locations",friend.hometown.name);

        if (friend.work!=undefined)
        {
            $.each(friend.work,function(work){
			    if (work.employer!=undefined)
                {
                    this.safe_add("locations",work.employer.name);
                }
            });
        }

        if (friend.education!=undefined)
        {
            $.each(friend.education,function(education){
                if (education.school!=undefined)
                {
                    this.safe_add("locations",education.school.name);
                }
            });
        }
    }

    this.suck_friend=function(friend,data) 
    {
	    var fb=this;
        data.people.push(friend.name);
 
	    FB.api("/"+friend.id, function(friend) {
	        fb.suck_from_friend(friend)
	    });	
       
        FB.api('/'+friend.id+'/books', function(books){
            if (books.data!=undefined) fb.suck_things(books.data,data);
        });
        FB.api('/'+friend.id+'/movies', function(movies) {
            if (movies.data!=undefined) fb.suck_things(movies.data,data);
        });
        
        //FB.api('/'+friend.id+'/albums', function(albums) {
        //    if (albums.data!=undefined) fb.suck_things(albums.data,data);
        //}); 
	    //FB.api('/'+friend.id+'/videos', function(videos) {
        //    if (videos.data!=undefined) fb.suck_things(videos.data,data);
        //});

    }

    this.suck_friends=function(data)
    {
	    var fb=this;
        FB.api('/me/friends', function(friends) {
            $.each(friends.data, function(index, friend) {
                fb.suck_friend(friend,data);
            })});
    }

    this.suck_me=function()
    {
        var fb=this;
        FB.api('/me', function(response) {
            fb.data.me = response;
            fb.refresh("me",response);
        });
    }

    this.suck = function()
    {
        debug("looking at facebook data...");
        this.suck_me();
        this.suck_friends(this.data);
    }

    this.init();
}
