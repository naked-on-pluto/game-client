
// converts a js object into text for displaying
function object_to_string(obj,depth)
{
    text="";
    for (i in obj)
    {
        if (typeof(obj[i])=="object")
        {           
            text+=ws(depth*4)+i+":\n"+object_to_string(obj[i],depth+1);
        }      
        else
        {
            text+=ws(depth*4)+i+":"+obj[i]+"\n";
        }
    }
    return text;
}

// add some html to the page
function debug(html)
{ 
    var div=document.createElement("div");   
    div.id = "foo";
    div.innerHTML = html;
    document.getElementById("people").appendChild(div);
}

// clear the page
function clear(id)
{
    var element=document.getElementById(id);
    while (element.firstChild) 
    {
        element.removeChild(element.firstChild);
    }
}

function facebook_status(html)
{
    clear('fb-status')
    var div=document.createElement("div");
    div.id = "fb-status";
    div.innerHTML = html;
    document.getElementById('fb-status').appendChild(div);
}
